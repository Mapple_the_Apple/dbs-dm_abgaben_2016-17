
public class Transporter extends Fahrzeuge{
	int lasterflaeche;
	int sitzplaetze;
	
	public Transporter(int baujahr, String hersteller, String kennzeichen, String farzeugtyp, int lasterflaeche, int sitzplaetze) {
		super(baujahr, hersteller, kennzeichen, farzeugtyp);
		this.lasterflaeche=lasterflaeche;
		this.sitzplaetze=sitzplaetze;
	}
	
	public void extra(){
		System.out.println("Lasterfläche: "+lasterflaeche+"m^2");
		System.out.println("Sitzplätze: "+sitzplaetze);

	}

}
