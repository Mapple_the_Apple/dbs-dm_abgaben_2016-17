
public abstract class Fahrzeuge {
	int baujahr;
	String hersteller;
	String kennzeichen;
	String farzeugtyp;
	/*int lasterflaeche;
	int sitzplaetze;
	String besonderheiten;
	int hubraum;*/
	
	public Fahrzeuge(int baujahr, String hersteller, String kennzeichen, String farzeugtyp){
		this.baujahr=baujahr;
		this.hersteller=hersteller;
		this.kennzeichen=kennzeichen;
		this.farzeugtyp=farzeugtyp;
	}
	public String getFahrzeug(){ return this.getClass().toString().substring(6);};
	public abstract void extra();
	
	void ausgabe(){
		System.out.println("Typ: "+getFahrzeug());
		System.out.println("Hersteller: "+hersteller);
		System.out.println("Modell: "+farzeugtyp);
		System.out.println("Kennzeichen: "+kennzeichen);
		System.out.println("Baujahr: "+baujahr);
		extra();
		System.out.println();


	}

}
