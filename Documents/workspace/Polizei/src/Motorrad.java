
public class Motorrad extends Fahrzeuge{
	int hubraum;
	
	public Motorrad(int baujahr, String hersteller, String kennzeichen, String farzeugtyp, int hubraum) {
		super(baujahr, hersteller, kennzeichen, farzeugtyp);
		this.hubraum=hubraum;
	}
	
	
	public void extra(){
		System.out.println("Hubraum: "+hubraum);
	}
}
