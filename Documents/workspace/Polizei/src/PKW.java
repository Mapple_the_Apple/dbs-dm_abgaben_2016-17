
public class PKW extends Fahrzeuge{
		String besonderheiten;

	public PKW(int baujahr, String hersteller, String kennzeichen, String farzeugtyp, String besonderheiten) {
		super(baujahr, hersteller, kennzeichen, farzeugtyp);
		this.besonderheiten=besonderheiten;
	}
	
	public void extra(){
		System.out.println("Besonderheiten: "+besonderheiten);
	}

}
