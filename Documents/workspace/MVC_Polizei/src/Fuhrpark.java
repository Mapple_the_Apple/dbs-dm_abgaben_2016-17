import java.util.ArrayList;

public class Fuhrpark {

	ArrayList<PKW> auto = new ArrayList<PKW>();
	Motorrad M3 = new PKW(2016,"BMW","IL 34HM","Cupe","Turbo");
	Motorrad H2R = new Motorrad(2017,"Kawasaki","IL 453M","H2R",998);
	Motorrad EXC = new Motorrad(2015,"KTM","IL 69XXX","EXC",450);
	Motorrad Scrambler = new Motorrad(2014,"Ducati","I 5XXX","Scrambler Desert Sled",800);
	Motorrad Monster = new Motorrad(2012,"Ducati","IL 0KAY","Monster",1200);
	Motorrad RNINET = new Motorrad(2016,"BMW","IL 98M","R NINE T",1200);
	Motorrad Superduke = new Motorrad(2016,"KTM","IL 365M","Superduke",1300);
	
	public Fuhrpark(){
		this.motorrad.add(GS);
		this.motorrad.add(H2R);
		this.motorrad.add(EXC);
		this.motorrad.add(Scrambler);
		this.motorrad.add(Monster);
		this.motorrad.add(RNINET);
		this.motorrad.add(Superduke);
	}

	private int auto = 4;
	
	ArrayList<Motorrad> motorrad = new ArrayList<Motorrad>();
	Motorrad GS = new Motorrad(2016,"BMW","IL 34HM","GS",1200);
	Motorrad H2R = new Motorrad(2017,"Kawasaki","IL 453M","H2R",998);
	Motorrad EXC = new Motorrad(2015,"KTM","IL 69XXX","EXC",450);
	Motorrad Scrambler = new Motorrad(2014,"Ducati","I 5XXX","Scrambler Desert Sled",800);
	Motorrad Monster = new Motorrad(2012,"Ducati","IL 0KAY","Monster",1200);
	Motorrad RNINET = new Motorrad(2016,"BMW","IL 98M","R NINE T",1200);
	Motorrad Superduke = new Motorrad(2016,"KTM","IL 365M","Superduke",1300);
	
	public Fuhrpark(){
		this.motorrad.add(GS);
		this.motorrad.add(H2R);
		this.motorrad.add(EXC);
		this.motorrad.add(Scrambler);
		this.motorrad.add(Monster);
		this.motorrad.add(RNINET);
		this.motorrad.add(Superduke);
	}
	
	
	int motorad=7;
	
	
	
	Transporter[] trans = new Transporter[2];
	private int transporter = 2;
	
	public String ausleihenAutos(){
		auto--;
		if(auto>0)return ("Auto ausgeliehen.");
		else return ("Kein Auto mehr da!");
	}
	public String ausleihenMotorrad(){
		motorad--;
		if(motorad>0)return ("Motorrad ausgeliehen.");
		else return ("Kein Motorrad mehr da!");
	}
	public String ausleihenTransporter(){
		transporter--;
		if(transporter>0)return ("Transporter ausgeliehen.");
		else return ("Kein Transporter mehr da!");
	}
	
	public String zurückgebenAutos(){
		auto++;
		if(auto<=4)return ("Auto zurückgegeben.");
		else return ("Alle Autos da!");
	}
	public String zurückgebenMotorrad(){
		motorad++;
		if(motorad<=7)return ("Motorrad zurückgegeben.");
		else return ("Alle Motorräder da!");
	}
	public String zurückgebenTransporter(){
		transporter++;
		if(transporter<=2)return ("Transporter zurückgegeben.");
		else return ("Alle Transporter schon da!");
	}
	

}
