public class UBoot extends Schiff {
	private int x1;
	private int y1;
	
	protected UBoot(int x1, int y1, Richtung.Orientierung o){
		super(x1,y1,o,4);
		this.x1=x1;
		this.y1=y1;
	}
	protected UBoot(int index, Richtung.Orientierung o, int l){
		super(index,o,4);
		this.x1=index-GameClass.abrunden.rund(index/GameClass.sx)*GameClass.sx;
		this.y1=GameClass.abrunden.rund(index/GameClass.sx);
	}
}
