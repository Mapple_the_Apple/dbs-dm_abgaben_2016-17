import java.util.*;

import javax.swing.*;

public abstract class Schiff {
	public Punkt ursprung;
	public String richtung;
	public Richtung.Orientierung o;
	public ArrayList<Punkt> getroffeneListe=new ArrayList <Punkt>();
	public ArrayList<Punkt> liste=new ArrayList <Punkt>();
	int l;

	public Schiff (int x1, int y1, Richtung.Orientierung o, int l){
		this.setUrsprung(x1-1,y1);
		this.o=o;
	}
	public Schiff (int index, Richtung.Orientierung o, int l){
		this.setUrsprung(index-GameClass.abrunden.rund(index/GameClass.sx)*GameClass.sx,GameClass.abrunden.rund(index/GameClass.sx));
		this.liste.add(new Punkt(index-GameClass.abrunden.rund(index/GameClass.sx)*GameClass.sx,GameClass.abrunden.rund(index/GameClass.sx)));

		if(o==Richtung.Orientierung.N){
			for(int i=1;i<=l;i++)
				this.liste.add(new Punkt(index-GameClass.abrunden.rund(index/GameClass.sx)*GameClass.sx,(GameClass.abrunden.rund(index/GameClass.sx))-GameClass.sx*i));
		}
		if(o==Richtung.Orientierung.O){
			for(int i=1;i<=l;i++)
				this.liste.add(new Punkt((index-GameClass.abrunden.rund(index/GameClass.sx)*GameClass.sx)+i,GameClass.abrunden.rund(index/GameClass.sx)));
		}

		this.o=o;
		this.l=l;
	}

	public Punkt getUrsprung(){
		return ursprung;
	}
	
	public int getLaenge(){
		return l;
	}
	
	private void setUrsprung(int x, int y){
		ursprung=new Punkt(x,y);
	}

	public void Erklärung(){
		System.out.println("Ich bin ein "+this.getClass().toString().substring(6)+" stehe auf den Kordinaten "+this.getUrsprung().toString()+" und bin "+this.getLaenge()+" Felder lang");
	}
	public String toString(){
		return("Ich bin ein "+this.getClass().toString().substring(6)+" stehe auf den Kordinaten "+this.getUrsprung().toString()+" und bin "+this.getLaenge()+" Felder lang, davon "+this.getroffeneListe.size() + "getroffen");
	}
	
	public void istGetroffen(Punkt p){
		if( liste.contains(p)) {
			getroffeneListe.add(p);
			liste.remove(p);
			//System.out.println(this.toString() + "getroffen");
		}
	}

	public boolean istZerstort(){
		//System.out.println(this.toString() + " wird geprüft");
		if(this.getroffeneListe.size()==this.getLaenge()){
			//System.out.println(this.toString()+ "zerstört");
			return true;
		}
		return false;
	}
}