int clockPin = 10;
int steuerPin = 11;
int clearPin = 12;

void setup()
{
  pinMode(clockPin, OUTPUT);
  digitalWrite(clockPin, LOW);
  pinMode(steuerPin, OUTPUT);
  digitalWrite(clearPin, HIGH);
  pinMode(clearPin, OUTPUT);
  digitalWrite(steuerPin, HIGH);
}

void loop()
{
  digitalWrite(clockPin, !digitalRead(clockPin));
  digitalWrite(steuerPin, HIGH);
}
