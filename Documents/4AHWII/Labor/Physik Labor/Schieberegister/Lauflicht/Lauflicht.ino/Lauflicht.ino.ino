int counter = 0;
int timer = 0;
int clockPin = 10;
int steuerPin = 11;
int clearPin = 12;

void setup()
{
  pinMode(clockPin, OUTPUT);
  digitalWrite(clockPin, LOW);
  pinMode(steuerPin, OUTPUT);
  digitalWrite(clearPin, HIGH);
  pinMode(clearPin, OUTPUT);
  digitalWrite(steuerPin, HIGH);
}

void loop()
{
  if(counter % 16 == 0)
  {
    digitalWrite(steuerPin, LOW);
  }
  digitalWrite(clockPin, !digitalRead(clockPin));
  digitalWrite(steuerPin, HIGH);
  delay(timer);
  if(counter == 16)
  {
    counter = 0;
    timer = 250;
  }
  counter++;
}
