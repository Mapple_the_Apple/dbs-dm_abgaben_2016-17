import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.*;
public class ClassWaggon {

	public static void main(String[] args) {

		System.out.println("Waggon Aufgabe");

		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:waggon.db"); 	 
			System.out.println("Opened database successfully");

			stmt = c.createStatement();
			String sql =	"CREATE TABLE KUNDEN" +
					"(KUNDENID 	INTEGER 	PRIMARY KEY AUTOINCREMENT,"+
					"NAME 		TEXT 		NOT NULL," +
					"SITZPLATZ 	INTEGER 	NOT NULL);";

			stmt.executeUpdate(sql);
			
			System.out.println("sql " + sql + " erfolgreich ausgeführt");

			sql= 	"CREATE TABLE SITZ2WAGGON" + 
					"(SITZPLATZID 	INTEGER 	PRIMARY KEY NOT NULL,"+ 
					"WAGGONID 		INTEGER 	NOT NULL);";

			stmt.executeUpdate(sql);
			
			System.out.println("sql " + sql + " erfolgreich ausgeführt");

			sql= "CREATE TABLE WAGGONKLASSEN"
					+ "(WAGGONID 			INTEGER 	NOT NULL,"
					+ "KLASSENBEZEICHNUNG 	TEXT 		NOT NULL);";	

			System.out.println("sql " + sql + " erfolgreich ausgeführt");

			stmt.executeUpdate(sql);
				
			
			sql= "SELECT "
					+ "k.NAME AS hugo,  w.KLASSENBEZEICHNUNG AS bez"
					+ "FROM"
					+ "KUNDEN k, WAGGONKLASSEN w, SITZ2WAGGON s"
					+ "WHERE"
					+ "w.WAGGONID = s.WAGGONID"
					+ "AND"
					+ "k.SITZPLATZ = s.SITZPLATZID"
					+ "ORDER BY"
					+ "k.NAME;";	

			ResultSet rs = stmt.executeQuery(sql);
			 while ( rs.next() ) {
				  String  sepp = rs.getString("hugo");
				  String  bez = rs.getString("bez");
				  
				  System.out.println("NAME: " + sepp + "Bezeichnung: " + bez);
			 }
			System.out.println("sql " + sql + " erfolgreich ausgeführt");

			sql= "SELECT NAME FROM KUNDEN WHERE SITZPLATZ IN"
					+ "(SELECT SITZPLATZID FROM SITZ2WAGGON WHERE WAGGONID IN"
					+ "(SELECT WAGGONID FROM WAGGONKLASSEN "
					+ "WHERE KLASSENBEZEICHNUNG = KLASSE_1));";

			System.out.println("sql " + sql + " erfolgreich ausgeführt");

			stmt.executeUpdate(sql);
			
			sql= "SELECT NAME FROM KUNDEN WHERE SITZPLATZ IN"
					+ "(SELECT SITZPLATZID FROM SITZ2WAGGON WHERE WAGGONID IN"
					+ "(SELECT WAGGONID FROM WAGGONKLASSEN "
					+ "WHERE KLASSENBEZEICHNUNG = KLASSE_2));";

			System.out.println("sql " + sql + " erfolgreich ausgeführt");
				
			stmt.executeUpdate(sql);
			
			
			stmt.executeUpdate(sql); //Enter bei sqlite3 
			stmt.close();
			c.close();
		}

		catch ( Exception e ) 
		{
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}
}



